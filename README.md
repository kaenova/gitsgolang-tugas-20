# Tugas 20

**Soal**: Buat Tabel Database secara otomatis dengan GORM.  
**Running**: Jalankan `docker-compose up -d` untuk menyalakan database. Setelah database menyala, bisa melakukan `go run main.go` pada directory ini. Nanti bisa dilihat menggunakan dbeaver bahwa ada tabel yang terbentuk.  

Masalah yang ku hadapi: Database tidak terinisialisasi sesuai dengan environment `POSTGRES_DB`. Masih belum melakukan relasi dengan tabel-tabel lain.

