package entity

import "github.com/google/uuid"

const (
	RakTableName = "rak"
)

type Rak struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid; primary_key"`
	Area string    `json:"nama" gorm:"type:varchar(50)"`
	// Buku[]
}

func NewRak(id uuid.UUID, area string) *Rak {
	return &Rak{
		ID:   id,
		Area: area,
	}
}
