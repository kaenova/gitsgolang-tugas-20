package entity

import "github.com/google/uuid"

const (
	BukuTableName = "buku"
)

type Buku struct {
	ID    uuid.UUID `json:"id" gorm:"type:uuid; primary_key"`
	Judul string    `json:"judul" gorm:"type:varchar(50);not_null"`
	Tahun int       `json:"tahun" gorm:"type:integer;not_null"`
	ISBN  int       `json:"isbn" gorm:"type:integer;not_null"`
}

func NewBuku(id uuid.UUID, judul string, tahun, isbn int) *Buku {
	return &Buku{
		ID:    id,
		Judul: judul,
		Tahun: tahun,
		ISBN:  isbn,
		// Author
		// Rak
	}
}
