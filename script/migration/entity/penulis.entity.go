package entity

import "github.com/google/uuid"

const (
	PenulisTableName = "penulis"
)

type Penulis struct {
	ID          uuid.UUID `json:"id" gorm:"type:uuid; primary_key"`
	Nama        string    `json:"nama" gorm:"type:varchar(50)"`
	TempatLahir string    `json:"tempatlahir" gorm:"varchar(50)"`
}

func NewPenulis(id uuid.UUID, nama, tempat string) *Penulis {
	return &Penulis{
		ID:          id,
		Nama:        nama,
		TempatLahir: tempat,
	}
}
