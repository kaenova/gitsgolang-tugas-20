package entity

import "github.com/google/uuid"

const (
	PegawaiTableName = "pegawai"
)

type Pegawai struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid; primary_key"`
	Nama string    `json:"nama" gorm:"type:varchar(50)"`
	HP   string    `json:"nomorhp" gorm:"varchar(50)"`
}

func NewPegawai(id uuid.UUID, nama, nomorhp string) *Peminjam {
	return &Peminjam{
		ID:   id,
		Nama: nama,
		HP:   nomorhp,
	}
}
