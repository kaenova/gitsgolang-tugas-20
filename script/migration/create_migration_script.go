package migration

import (
	"latihan_20/script/migration/entity"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

func InitMigration(db *gorm.DB) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	db.AutoMigrate(&entity.Buku{}, &entity.Pegawai{}, &entity.Peminjam{}, &entity.Penulis{}, &entity.Rak{})
}
