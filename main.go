package main

import (
	"latihan_20/db"

	"github.com/joho/godotenv"
)

func main() {
	// Init environment
	godotenv.Load()
	// Init DB
	db.Init()
}
