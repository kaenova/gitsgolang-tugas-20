package db

import (
	"fmt"
	"latihan_20/internal/config"
	"latihan_20/script/migration"

	"github.com/rs/zerolog/log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

func Init() {
	config, err := config.GetConfig()

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Database.Host,
		config.Database.Port,
		config.Database.Username,
		config.Database.Password,
		config.Database.Name)
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{SkipDefaultTransaction: true})
	checkError(err)

	fmt.Println("================== Initializing DB ==================")
	fmt.Println("Migrating...")
	migration.InitMigration(db)

	fmt.Println("Connected to Database")
	fmt.Println("================== DB Initialized ==================")
}

func CreateCon() *gorm.DB {
	return db
}

func checkError(err error) {
	if err != nil {
		log.Fatal().Err(err)
		panic(err)
	}
}
