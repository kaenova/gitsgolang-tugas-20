module latihan_20

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/rs/zerolog v1.15.0
	gorm.io/driver/postgres v1.1.2
	gorm.io/gorm v1.21.15
)
